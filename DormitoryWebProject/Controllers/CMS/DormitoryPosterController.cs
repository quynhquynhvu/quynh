﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DormitoryWebProject.Models;

namespace DormitoryWebProject.Controllers.CMS
{
    [RoutePrefix("CMS")]
    public class DormitoryPosterController : Controller
    {
        private DormitoryDBContext db = new DormitoryDBContext();

        // GET: CMS/DormitoryPoster
        [Route("DormitoryPoster")]
        public ActionResult Index()
        {
            ViewData["aCMS"] = "active";
            return View(db.Poster.ToList());
        }

        // GET:CMS/DormitoryPoster/Details/5
        [Route("DormitoryPoster/Details")]
        public ActionResult Details(int? id)
        {
            ViewData["aCMS"] = "active";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Poster poster = db.Poster.Find(id);
            if (poster == null)
            {
                return HttpNotFound();
            }
            return View(poster);
        }

        // GET: CMS/DormitoryPoster/Create
        [Route("DormitoryPoster/Create")]
        public ActionResult Create()
        {
            ViewData["aCMS"] = "active";
            return View();
        }

        // POST: CMS/DormitoryPoster/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Caption,pictureUpload")] Poster poster)
        {

            if (ModelState.IsValid)
            {
                //Picture Upload Handle
                string filename = Path.GetFileName(poster.pictureUpload.FileName);
                string path = Path.Combine(Server.MapPath("~/assets/images/CMS/poster"), filename);
                poster.pictureUpload.SaveAs(path);
                string pathInDb = "/assets/images/CMS/poster/" + filename;
                poster.picturePath = pathInDb;

                poster.LastModifiedDate = DateTime.Now;
                db.Poster.Add(poster);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(poster);
        }

        // GET: CMS/DormitoryPoster/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewData["aCMS"] = "active";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Poster poster = db.Poster.Find(id);
            if (poster == null)
            {
                return HttpNotFound();
            }
            return View(poster);
        }

        // POST: CMS/DormitoryPoster/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Caption,pictureUpload")] Poster poster)
        {
            if (ModelState.IsValid)
            {
                //Picture Upload Handle
                string filename = Path.GetFileName(poster.pictureUpload.FileName);
                string path = Path.Combine(Server.MapPath("~/assets/images/CMS/poster"), filename);
                poster.pictureUpload.SaveAs(path);
                string pathInDb = "/assets/images/CMS/poster/" + filename;
                poster.picturePath = pathInDb;

                poster.LastModifiedDate = DateTime.Now;
                db.Entry(poster).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(poster);
        }

        // GET: CMS/DormitoryPoster/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewData["aCMS"] = "active";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Poster poster = db.Poster.Find(id);
            if (poster == null)
            {
                return HttpNotFound();
            }
            return View(poster);
        }

        // POST: CMS/DormitoryPoster/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Poster poster = db.Poster.Find(id);
            db.Poster.Remove(poster);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
