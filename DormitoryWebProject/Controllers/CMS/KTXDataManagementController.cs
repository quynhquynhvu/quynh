﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DormitoryWebProject.Models;

namespace DormitoryWebProject.Controllers.CMS
{
    [RoutePrefix("CMS")]
    public class KTXDataManagementController : Controller
    {
        private DormitoryDBContext db = new DormitoryDBContext();

        // GET: KTXDataManagement
        public ActionResult Index()
        {
            ViewData["aCMS"] = "active";
            return View(db.KTX.ToList());
        }

        // GET: KTXDataManagement/Details/5
        public ActionResult Details(int? id)
        {
            ViewData["aCMS"] = "active";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KTX kTX = db.KTX.Find(id);
            if (kTX == null)
            {
                return HttpNotFound();
            }
            return View(kTX);
        }

        // GET: KTXDataManagement/Create
        public ActionResult Create()
        {
            ViewData["aCMS"] = "active";
            return View();
        }

        // POST: KTXDataManagement/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KtxID,TenKTX,DiaChi,SDTLienHe,Email")] KTX kTX)
        {
            if (ModelState.IsValid)
            {
                db.KTX.Add(kTX);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kTX);
        }

        // GET: KTXDataManagement/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewData["aCMS"] = "active";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KTX kTX = db.KTX.Find(id);
            if (kTX == null)
            {
                return HttpNotFound();
            }
            return View(kTX);
        }

        // POST: KTXDataManagement/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KtxID,TenKTX,DiaChi,SDTLienHe,Email")] KTX kTX)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kTX).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kTX);
        }

        // GET: KTXDataManagement/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewData["aCMS"] = "active";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KTX kTX = db.KTX.Find(id);
            if (kTX == null)
            {
                return HttpNotFound();
            }
            return View(kTX);
        }

        // POST: KTXDataManagement/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KTX kTX = db.KTX.Find(id);
            db.KTX.Remove(kTX);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
