﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DormitoryWebProject.Models;

namespace DormitoryWebProject.Controllers.CMS
{
    [RoutePrefix("CMS")]
    public class TestimonialsController : Controller
    {
        private DormitoryDBContext db = new DormitoryDBContext();

        // GET: Testimonials
        public ActionResult Index()
        {
            ViewData["aCMS"] = "active";
            var testimonials = db.Testimonial.Include(t => t.Khoa);
            return View(testimonials.ToList());
        }

        // GET: Testimonials/Details/5
        public ActionResult Details(int? id)
        {
            ViewData["aCMS"] = "active";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Testimonial testimonial = db.Testimonial.Find(id);
            if (testimonial == null)
            {
                return HttpNotFound();
            }
            return View(testimonial);
        }

        // GET: Testimonials/Create
        public ActionResult Create()
        {
            ViewData["aCMS"] = "active";
            ViewBag.KhoaFK = new SelectList(db.Khoa, "KhoaID", "SoKhoa");
            return View();
        }

        // POST: Testimonials/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TestimonialID,TenKhachHang,BinhLuan,CongViec,Rated,pictureUpload,KhoaFK")] Testimonial testimonial)
        {
            if (ModelState.IsValid)
            {
                //Picture Upload Handle
                string filename = Path.GetFileName(testimonial.pictureUpload.FileName);
                string path = Path.Combine(Server.MapPath("~/assets/images/CMS/client"), filename);
                testimonial.pictureUpload.SaveAs(path);
                string pathInDb = "/assets/images/CMS/client/" + filename;
                testimonial.picturePath = pathInDb;

                db.Testimonial.Add(testimonial);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KhoaFK = new SelectList(db.Khoa, "KhoaID", "SoKhoa", testimonial.KhoaFK);
            return View(testimonial);
        }

        // GET: Testimonials/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewData["aCMS"] = "active";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Testimonial testimonial = db.Testimonial.Find(id);
            if (testimonial == null)
            {
                return HttpNotFound();
            }
            ViewBag.KhoaFK = new SelectList(db.Khoa, "KhoaID", "SoKhoa", testimonial.KhoaFK);
            return View(testimonial);
        }

        // POST: Testimonials/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TestimonialID,TenKhachHang,BinhLuan,CongViec,Rated,pictureUpload,KhoaFK")] Testimonial testimonial)
        {
            if (ModelState.IsValid)
            {
                //Picture Upload Handle
                string filename = Path.GetFileName(testimonial.pictureUpload.FileName);
                string path = Path.Combine(Server.MapPath("~/assets/images/CMS/client"), filename);
                testimonial.pictureUpload.SaveAs(path);
                string pathInDb = "/assets/images/CMS/client/" + filename;
                testimonial.picturePath = pathInDb;

                db.Entry(testimonial).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KhoaFK = new SelectList(db.Khoa, "KhoaID", "SoKhoa", testimonial.KhoaFK);
            return View(testimonial);
        }

        // GET: Testimonials/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewData["aCMS"] = "active";
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Testimonial testimonial = db.Testimonial.Find(id);
            if (testimonial == null)
            {
                return HttpNotFound();
            }
            return View(testimonial);
        }

        // POST: Testimonials/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Testimonial testimonial = db.Testimonial.Find(id);
            db.Testimonial.Remove(testimonial);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
