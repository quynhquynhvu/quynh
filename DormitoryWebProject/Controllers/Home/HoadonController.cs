﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DormitoryWebProject.Controllers
{
    [RoutePrefix("Home/Hoadon")]
    public class HoadonController : Controller
    {
        //GET: Home/Hoadon/Hoadonlephi
        public ActionResult Hoadonlephi()
        {
            ViewData["aHD"] = "active";
            ViewData["aHDLP"] = "active";
            return View();
        }

        //GET: Home/Hoadon/Hoadondiennuoc
        public ActionResult Hoadondiennuoc()
        {
            ViewData["aHD"] = "active";
            ViewData["aHDDN"] = "active";
            return View();
        }
    }
}