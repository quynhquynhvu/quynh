﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DormitoryWebProject.Models
{
    public class DormitoryDBContext : DbContext
    {
        public DormitoryDBContext()
            : base("DefaultConnection")
        {

        }
        public DbSet<KTX> KTX { get; set; }
        public DbSet<ChiTietKTX> ChiTietKTX { get; set; }
        public DbSet<Tang> Tang { get; set; }
        public DbSet<Loai> Loai { get; set; }
        public DbSet<Phong> Phong { get; set; }
        public DbSet<ChiTietPhong> ChiTietPhong { get; set; }
        public DbSet<ChucVu> ChucVu { get; set; }
        public DbSet<School> School { get; set; }
        public DbSet<Lop> Lop { get; set; }
        public DbSet<Khoa> Khoa { get; set; }
        public DbSet<SinhVien> SinhVien { get; set; }
        public DbSet<Poster> Poster { get; set; }
        public DbSet<Testimonial> Testimonial { get; set; }
    }
}