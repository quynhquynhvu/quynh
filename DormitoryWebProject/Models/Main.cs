﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DormitoryWebProject.Models
{
    //Primary
    public class KTX
    {
        [Key]
        [DisplayName("Mã KTX")]
        public int KtxID { get; set; }
        [DisplayName("Tên KTX")]
        public string TenKTX { get; set; }
        [DisplayName("Địa chỉ")]
        public string DiaChi { get; set; }
        [DisplayName("Số điện thoại liên hệ")]
        public int SDTLienHe { get; set; }
        public string Email { get; set; }

        public virtual ICollection<Phong> Phongs { get; set; }
        public virtual ICollection<PhieuGiatSay> PhieuGiatSays { get; set; }
    }

    public class ChiTietKTX
    {
        [Key]
        public int CTKTXID { get; set; }

        [DisplayName("Đánh giá")]
        [Range(1, 5)]
        public double rated { get; set; }
        [DisplayName("Giá thuê thấp nhất")]
        public double GiaThueThapNhat { get; set; }
        [DisplayName("Giá thuê cao nhất")]
        public double GiaThueCaoNhat { get; set; }
        [DisplayName("Mô tả KTX")]
        [MinLength(200, ErrorMessage = "Độ dài mô tả tối thiểu 200 kí tự!")]
        public string MoTaKTX { get; set; }
        public string picturePath1 { get; set; }
        public string picturePath2 { get; set; }
        public string picturePath3 { get; set; }
        public string picturePath4 { get; set; }
        public string picturePath5 { get; set; }
        [ForeignKey("KTX")]
        [DisplayName("Kí túc xá")]
        public int KtxFK { get; set; }

        [NotMapped]
        [DisplayName("Hình ảnh KTX 1")]
        public HttpPostedFileBase pictureUpload1 { get; set; }
        [NotMapped]
        [DisplayName("Hình ảnh KTX 2")]
        public HttpPostedFileBase pictureUpload2 { get; set; }
        [NotMapped]
        [DisplayName("Hình ảnh KTX 3")]
        public HttpPostedFileBase pictureUpload3 { get; set; }
        [NotMapped]
        [DisplayName("Hình ảnh KTX 4")]
        public HttpPostedFileBase pictureUpload4 { get; set; }
        [NotMapped]
        [DisplayName("Hình ảnh KTX 5")]
        public HttpPostedFileBase pictureUpload5 { get; set; }

        public virtual KTX KTX { get; set; }
    }

    public class Tang
    {
        [Key]
        [DisplayName("Mã tầng")]
        public int TangID { get; set; }
        [DisplayName("Tầng")]
        public string Ten { get; set; }

        public virtual ICollection<Phong> Phongs { get; set; }
    }

    public class Loai
    {
        [Key]
        [DisplayName("Mã loại")]
        public int LoaiID { get; set; }
        [DisplayName("Loại phòng")]
        public string TenLoai { get; set; }

        public virtual ICollection<Phong> Phongs { get; set; }
    }

    public class ChucVu
    {
        [Key]
        [DisplayName("Mã chức vụ")]
        public int ChucVuID { get; set; }
        [DisplayName("Tên chức vụ")]
        public string TenChucVu { get; set; }

        public virtual ICollection<ChiTietPhong> ChiTietPhongs { get; set; }
    }

    public class School
    {
        [Key]
        [DisplayName("Mã ID")]
        public int SchoolID { get; set; }
        [DisplayName("Tên khoa")]
        public string SchoolName { get; set; }

        public virtual ICollection<Lop> Lops { get; set; }
    }

    public class Khoa
    {
        [Key]
        [DisplayName("Mã ID")]
        public int KhoaID { get; set; }
        [DisplayName("Khóa")]
        public string SoKhoa { get; set; }

        public virtual ICollection<SinhVien> SinhViens { get; set; }
        public virtual ICollection<Testimonial> Testimonials { get; set; }
    }

    public class Lop
    {
        [Key]
        [DisplayName("Mã ID")]
        public int LopID { get; set; }
        [DisplayName("Mã lớp")]
        public string MaLop { get; set; }
        [DisplayName("Tên lớp")]
        public string TenLop { get; set; }

        [ForeignKey("School")]
        public int SchoolFK { get; set; }

        public virtual School School { get; set; }
        public virtual ICollection<SinhVien> SinhViens { get; set; }
    }
    public class Phong
    {
        [Key]
        [DisplayName("Mã phòng")]
        public int PhongID { get; set; }
        [DisplayName("Tên phòng")]
        public string TenPhong { get; set; }
        [DisplayName("Tổng số giường")]
        public int TongSoGiuong { get; set; }
        [DisplayName("Cơ sở vật chất")]
        public string CoSoVatChat { get; set; }

        public string picturePath1 { get; set; }
        public string picturePath2 { get; set; }
        public string picturePath3 { get; set; }
        public string picturePath4 { get; set; }
        public string picturePath5 { get; set; }

        [NotMapped]
        [DisplayName("Hình ảnh phòng 1")]
        public HttpPostedFileBase pitureUpload1 { get; set; }
        [NotMapped]
        [DisplayName("Hình ảnh phòng 2")]
        public HttpPostedFileBase pitureUpload2 { get; set; }
        [NotMapped]
        [DisplayName("Hình ảnh phòng 3")]
        public HttpPostedFileBase pitureUpload3 { get; set; }
        [NotMapped]
        [DisplayName("Hình ảnh phòng 4")]
        public HttpPostedFileBase pitureUpload4 { get; set; }
        [NotMapped]
        [DisplayName("Hình ảnh phòng 5")]
        public HttpPostedFileBase pitureUpload5 { get; set; }

        [ForeignKey("KTX")]
        public int KtxFK { get; set; }
        [ForeignKey("Tang")]
        public int TangFK { get; set; }
        [ForeignKey("Loai")]
        public int LoaiFK { get; set; }

        public virtual KTX KTX { get; set; }
        public virtual Tang Tang { get; set; }
        public virtual Loai Loai { get; set; }

    }

    public class ChiTietPhong
    {
        [Key]
        [ForeignKey("SinhVien")]
        [DisplayName("Mã CTP ID")]
        public int CTPID { get; set; }
        [DisplayName("Ngày vào")]
        public DateTime NgayVao { get; set; }

        [ForeignKey("Phong")]
        public int PhongFK { get; set; }
        [ForeignKey("ChucVu")]
        public int ChucVuFK { get; set; }

        public virtual Phong Phong { get; set; }
        
        public virtual SinhVien SinhVien { get; set; }
        public virtual ChucVu ChucVu { get; set; }
    }

    public class SinhVien
    {
        [Key]
        [DisplayName("Mã sinh viên")]
        public int SinhVienID { get; set; }
        [DisplayName("Tên sinh viên")]
        public string TenSV { get; set; }
        [DisplayName("Ngày sinh")]
        public DateTime NgaySinh { get; set; }
        [DisplayName("Số CMND")]
        public string CMND { get; set; }
        [DisplayName("Số điện thoại")]
        public int SDT { get; set; }
        [DisplayName("Quê quán")]
        public string QueQuan { get; set; }
        [DisplayName("Giới thiệu bản thân")]
        public string TuGioiThieu { get; set; }
        [DisplayName("Giới tính")]
        public bool GioiTinh { get; set; }

        public string piturePath { get; set; }
        [NotMapped]
        [DisplayName("Hình ảnh cá nhân")]
        public HttpPostedFileBase pitureUpload { get; set; }

        [ForeignKey("Khoa")]
        public int KhoaFK { get; set; }
        [ForeignKey("Lop")]
        public int LopFK { get; set; }

        public virtual Khoa Khoa { get; set; }
        public virtual Lop Lop { get; set; }

        public virtual ICollection<ChiTietPhieuGiat> ChiTietPhieuGiats { get; set; }
        public virtual ChiTietPhong ChiTietPhong { get; set; }
    }

    public class PhieuGiatSay
    {
        [Key]
        [DisplayName("Mã phiếu giặt")]
        public int PhieuGSID { get; set; }
        [DisplayName("Thành tiền")]
        public double ThanhTien { get; set; }
        [DisplayName("Trạng thái")]
        public double TrangThai { get; set; }
        [DisplayName("Ngày nhận")]
        public DateTime NgayNhan { get; set; }
        [DisplayName("Ngày trả dự kiến")]
        public DateTime NgayTraDuKien { get; set; }
        [ForeignKey("KTX")]
        public int KtxFK { get; set; }

        public virtual ICollection<ChiTietPhieuGiat> ChiTietPhieuGiats { get; set; }
        public virtual KTX KTX { get; set; }
    }

    public class ChiTietPhieuGiat
    {
        [Key]
        [DisplayName("Mã chi tiết")]
        public int CTPID { get; set; }
        [DisplayName("Số lượng")]
        public int SoLuong { get; set; }
        [DisplayName("Đơn giá")]
        public int DonGia { get; set; }
        [ForeignKey("SinhVien")]
        public int SinhVienFK { get; set; }
        [ForeignKey("PhieuGiatSay")]
        public int PhieuGSFK { get; set; }
        
        public virtual SinhVien SinhVien { get; set; }
        public virtual PhieuGiatSay PhieuGiatSay { get; set; }
    }

    //Appendix
    public class Poster
    {
        [Key]
        [DisplayName("Mã ID")]
        public int ID { get; set; }
        [Required]
        public string Caption { get; set; }

        [DisplayName("Ngày sửa cuối cùng")]
        public DateTime LastModifiedDate { get; set; }

        public string picturePath { get; set; }
        [NotMapped]
        [DisplayName("Hình ảnh")]
        public HttpPostedFileBase pictureUpload { get; set; }
    }

    public class Testimonial
    {
        [Key]
        public int TestimonialID { get; set; }
        [DisplayName("Tên khách hàng")]
        public string TenKhachHang { get; set; }
        [DisplayName("Bình luận")]
        public string BinhLuan { get; set; }
        [DisplayName("Công việc")]
        public string CongViec { get; set; }
        [DisplayName("Đánh giá")]
        [Range(1, 5)]
        public double Rated { get; set; }
        public string picturePath { get; set; }

        [NotMapped]
        [DisplayName("Hình ảnh cá nhân")]
        public HttpPostedFileBase pictureUpload { get; set; }
        [ForeignKey("Khoa")]
        public int KhoaFK { get; set; }

        public virtual Khoa Khoa { get; set; }
    }
}